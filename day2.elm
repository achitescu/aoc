import Html exposing(..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Char

main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , view = view
        , update = update
    }

type alias Model = 
    { input : String
    , response : Int
    , part : Part
    }

type Part = Part1 | Part2
type Msg = Input String
    | SwitchTo Part

initModel : Model
initModel = { input = "", response = 0, part=Part1}

update : Msg -> Model -> Model
update msg model= 
    case msg of 
        Input aoc_input->
            let 
                solveFunction =  
                        case model.part of 
                            Part1 -> solve
                            Part2 -> solve2
                parsed_input =  parse aoc_input
                r = solveFunction parsed_input
            in 
                { model | input = aoc_input
                , response = r }
        SwitchTo p ->
            {model | part = p}

view : Model -> Html Msg
view model = 
    div []
        [ 
          fieldset []
            [ radio "Part1" (SwitchTo Part1)
            , radio "Part2" (SwitchTo Part2)
            ]
        , Html.textarea [ placeholder "Advent of code input", onInput Input] []
        , div [] [text (toString model.response)]
        ]

radio : String -> msg -> Html msg
radio value msg =
  label
    [ style [("padding", "20px")]
    ]
    [ input [ type_ "radio", name "font-size", onClick msg ] []
    , text value
    ]



default0 : String -> Int
default0 x= Result.withDefault 0 (String.toInt x)
numbers : String -> List Int
numbers input = 
    input 
        |> String.words
        |> List.map default0

parse : String -> List (List Int)
parse user_input = 
    user_input 
        |> String.split "\n"
        |> List.map numbers

rowChecksum: (List Int) -> Int
rowChecksum l = 
    let 
        max = Maybe.withDefault 0 (List.maximum l)
        min = Maybe.withDefault 0 (List.minimum l)
    in 
        max-min

solve:List (List Int) -> Int
solve matrix = 
    matrix
        |> List.map rowChecksum
        |> List.sum



divisible : Int -> Int -> Bool
divisible a b = ((a % b) == 0 ) || ((b % a) == 0)
evenlyDivisible: Int -> (List Int) -> Int
evenlyDivisible n l =
    let 
        filter_func = divisible n
        filtered = List.filter  filter_func l
    in
        case List.head filtered of 
            Just x -> 
                    if n > x then 
                        n // x 
                    else 
                        x // n
            Nothing -> 0

rowDivide: (List Int) -> Int
rowDivide row = 
    case row of 
        (n::ns) -> 
            let 
                aux = evenlyDivisible n ns
            in
                if aux == 0 then
                    rowDivide ns
                else 
                    aux
        [] -> 0

solve2: List (List Int) -> Int
solve2 matrix = 
    matrix
        |> List.map rowDivide
        |> List.sum

