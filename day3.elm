import Html exposing(..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)

main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , view = view
        , update = update
    }

type alias Model = 
    { input : String
    , response : Int
    , part : Part
    }

type Part = Part1 | Part2
type Msg = Input String
    | SwitchTo Part

initModel : Model
initModel = { input = "", response = 0, part=Part1}

update : Msg -> Model -> Model
update msg model= 
    case msg of 
        Input aoc_input->
            let 
                solveFunction =  
                        case model.part of 
                            Part1 -> solve
                            Part2 -> solve
                parsed_input =  parse aoc_input
                r = solveFunction parsed_input
            in 
                { model | input = aoc_input
                , response = r }
        SwitchTo p ->
            {model | part = p}

view : Model -> Html Msg
view model = 
    div []
        [ 
          fieldset []
            [ radio "Part1" (SwitchTo Part1)
            , radio "Part2" (SwitchTo Part2)
            ]
        , Html.textarea [ placeholder "Advent of code input", onInput Input] []
        , div [] [text (toString model.response)]
        ]

radio : String -> msg -> Html msg
radio value msg =
  label
    [ style [("padding", "20px")]
    ]
    [ input [ type_ "radio", name "font-size", onClick msg ] []
    , text value
    ]

parse : String -> Int
parse user_input = Result.withDefault 0 (String.toInt user_input)

isEven: Int -> Bool
isEven number=
    number % 2 == 0

solve: Int -> Int
solve location = 
    let 
        rad = (floor << sqrt << toFloat) location
        edge_length =  if (isEven << floor << sqrt << toFloat) location then 
                rad + 1
            else
                rad + 2
        prev = (edge_length - 2)^2 
        pos = location - prev
        distance_to_center = (edge_length // 2)
        pos_in_edge = pos % (edge_length - 1)
    in 
        Debug.log ("location=" ++ (toString location) )
        Debug.log (toString rad)
        Debug.log (toString edge_length)
        Debug.log (toString prev)
        Debug.log (toString distance_to_center)
        abs (pos_in_edge - (edge_length - 1)// 2) + distance_to_center    
