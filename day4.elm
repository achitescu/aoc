import Html exposing(..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)

main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , view = view
        , update = update
    }

type alias Model = 
    { input : String
    , response : Int
    , part : Part
    }

type alias AOCInput = List (List String)
type Part = Part1 | Part2
type Msg = Input String
    | SwitchTo Part

initModel : Model
initModel = { input = "", response = 0, part=Part1}

update : Msg -> Model -> Model
update msg model= 
    case msg of 
        Input aoc_input->
            let 
                solveFunction =  
                        case model.part of 
                            Part1 -> solve
                            Part2 -> solve2
                parsed_input =  parse aoc_input
                r = solveFunction parsed_input
            in 
                { model | input = aoc_input
                , response = r }
        SwitchTo p ->
            {model | part = p}

view : Model -> Html Msg
view model = 
    div []
        [ 
          fieldset []
            [ radio "Part1" (SwitchTo Part1)
            , radio "Part2" (SwitchTo Part2)
            ]
        , Html.textarea [ placeholder "Advent of code input", onInput Input] []
        , div [] [text (toString model.response)]
        ]

radio : String -> msg -> Html msg
radio value msg =
  label
    [ style [("padding", "20px")]
    ]
    [ input [ type_ "radio", name "font-size", onClick msg ] []
    , text value
    ]

parse : String -> AOCInput
parse user_input = 
    user_input 
        |> String.split "\n"
        |> List.map String.words


sortWord:String -> String
sortWord word= 
    word 
        |> String.toList
        |> List.sort
        |> String.fromList

validAnagramPassphrase: List String -> Int
validAnagramPassphrase pp =
    pp 
        |> List.map sortWord
        |> validPassPhrase

validPassPhrase: List String -> Int
validPassPhrase pp = 
    case pp of 
        (w::ws) -> 
            if (List.member w ws) then 
                0
            else
                Debug.log ("valid "++ (toString pp))  (validPassPhrase ws)
        [] -> 1

solve : AOCInput -> Int
solve input =
    input
        |> List.map validPassPhrase
        |> List.foldr (+) 0 




solve2 : AOCInput -> Int
solve2 input = 
    input
        |> List.map validAnagramPassphrase
        |> List.foldr (+) 0 

