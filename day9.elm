import Html exposing(..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import List exposing(..)

main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , view = view
        , update = update
    }

type alias Model = 
    { input : String
    , response : Int
    , part : Part
    }

type Part = Part1 | Part2
type Msg = Input String
    | SwitchTo Part

type alias Stream = List Char

initModel : Model
initModel = { input = "", response = 0, part=Part1}

update : Msg -> Model -> Model
update msg model= 
    case msg of 
        Input aoc_input->
            let 
                solveFunction =  
                        case model.part of 
                            Part1 -> solve
                            Part2 -> solve2
                parsed_input =  parse aoc_input
                r = solveFunction parsed_input
            in 
                { model | input = aoc_input
                , response = r }
        SwitchTo p ->
            {model | part = p}

view : Model -> Html Msg
view model = 
    div []
        [ 
          fieldset []
            [ radio "Part1" (SwitchTo Part1)
            , radio "Part2" (SwitchTo Part2)
            ]
        , Html.textarea [ placeholder "Advent of code input", onInput Input] []
        , div [] [text (toString model.response)]
        ]

radio : String -> msg -> Html msg
radio value msg =
  label
    [ style [("padding", "20px")]
    ]
    [ input [ type_ "radio", name "font-size", onClick msg ] []
    , text value
    ]

parse : String -> Stream
parse = String.toList

garbage : Stream -> Stream
garbage  stream =
    let 
        current_char = Maybe.withDefault 'x' (head stream)
        nstream = Maybe.withDefault [] (tail stream)
    in 
        case Debug.log "garbage" current_char of 
            '>' -> nstream
            '!' -> garbage (Maybe.withDefault [] (tail nstream))
            'x' -> []
            _ -> garbage nstream

garbageCount : Stream -> Int
garbageCount  stream =
    let 
        current_char = Maybe.withDefault 'x' (head stream)
        nstream = Maybe.withDefault [] (tail stream)
    in 
        case Debug.log "garbageCount" current_char of 
            '>' -> solve2 nstream
            '!' -> garbageCount (Maybe.withDefault [] (tail nstream))
            'x' -> 0
            _ -> 1 + garbageCount nstream


group : Int -> Stream -> Int
group n stream =
    if n == 0 then 
        (solve << Maybe.withDefault [] << tail) stream
    else 
        let 
            current_char = Maybe.withDefault 'x' (head stream)
            nstream = Maybe.withDefault [] (tail stream)
        in 
            --Debug.log "group " ++ (toString n)  ++ (toString current_char) 
            case Debug.log ("group " ++ (toString n)) current_char of 
                '{' -> 
                    group (n+1)  nstream
                '}' -> 
                    n + (group (n-1) nstream)
                --'<' ->
                    --group
                '<' ->
                    group n (garbage nstream)
                'x' -> 0
                _ -> 
                    group n nstream
     
solve: Stream -> Int
solve stream = 
    --Debug.log "a" 2
    let 
        current_char = Maybe.withDefault 'x' (head stream)
        nstream = Maybe.withDefault [] (tail stream)
    in

        case Debug.log "solve" current_char of 
            '{' -> 
                group 1  nstream 
            '<' ->
                solve (garbage nstream)
            'x' -> 0
            _ -> 
                solve nstream
 
solve2: Stream -> Int
solve2 stream = 
    --Debug.log "a" 2
    let 
        current_char = Maybe.withDefault 'x' (head stream)
        nstream = Maybe.withDefault [] (tail stream)
    in

        case Debug.log "solve" current_char of 
            '<' ->
                garbageCount nstream
                --let 
                --    gc = garbageCount nstream
                --in
                --   Debug.log "solve gc="  gc + solve2 (drop gc nstream) 
            'x' -> 0
            _ -> 
                solve2 nstream
 