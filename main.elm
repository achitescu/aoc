import Html exposing(..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Char

main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , view = view
        , update = update
    }

type alias Model = 
    { input : String
    , response : List Char
    , part : Part
    }

type Part = Part1 | Part2
type Msg = Input String
    | SwitchTo Part

initModel : Model
initModel = { input = "", response = [], part=Part1}

update : Msg -> Model -> Model
update msg model= 
    case msg of 
        Input aoc_input->
            let 
                solveFunction =  
                    case model.part of 
                        Part1 -> solve
                        Part2 -> solveN            
                r = solveFunction aoc_input
            in 
                { model | input = aoc_input
                , response = r }
        SwitchTo p ->
            {model | part = p}

view : Model -> Html Msg
view model = 
    div []
        [ fieldset []
            [ radio "Part1" (SwitchTo Part1)
            , radio "Part2" (SwitchTo Part2)
            ]
        , input [ placeholder "Advent of code input", onInput Input] []
        , div [] [text (String.fromList model.response)
        , div [] [text (toString (sumChar model.response))]]
        ]

radio : String -> msg -> Html msg
radio value msg =
  label
    [ style [("padding", "20px")]
    ]
    [ input [ type_ "radio", name "font-size", onClick msg ] []
    , text value
    ]

sumChar : List Char ->  Int
sumChar l = 
    l 
        |> List.map (\c -> -48 + Char.toCode c)
        |> List.foldr (+) 0

solve : String -> List Char
solve input = 
    let 
        charlist = String.toLis t input
        first = List.head charlist
    in   
        case first of 
            Nothing -> []
            Just val ->
                --val :: (List.reverse charlist)
                captcha (val::charlist)

solveN : String -> List Char
solveN input =
    let 
        mid = (String.length input) // 2
        charlist = String.toList input
    in
        Debug.log (toString (List.take mid charlist))
        captchaN (List.append charlist (List.take mid charlist)) mid


captcha : List Char -> List Char
captcha  input = 
    case input of 
        d::ds ->
            let 
                second = List.head ds
                ncaptcha = captcha ds
            in
                case second of
                    Nothing -> []
                    Just val -> 
                        if d == val then
                            d :: ncaptcha
                        else 
                            ncaptcha
        [] -> []


captchaN : List Char -> Int -> List Char
captchaN input n = 
    case input of
        d::ds ->
            let 
                nth = ds 
                        |> List.drop (n-1)
                        |> List.head
                        |> Maybe.withDefault 'x'
                x = Debug.log (toString  nth)
                ncaptcha = captchaN ds n
            in 
                if d == nth then
                    d::ncaptcha
                else
                    ncaptcha
        [] -> []